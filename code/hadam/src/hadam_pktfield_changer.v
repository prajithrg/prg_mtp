///////////////////////////////////////////////////////////////////////////////
// 
// Author: Prajith R G <rg.prajith@gmail.com>
// Module: hadam_pktfield_changer.v
// Project: HADAM
// Description: The FSM is implemented to read the NetFPGA words [8 byte] one
// by one in each clock [125MHz ~ 8ns] and then the words are parsed to update
// the packet fields.  This module also takes as input old TCP checksum, 
// src ip, dest ip, src port and dest port from the hadam_puzzle_setter module
// and computes the updated values and replaces those in the packet fields on
// the fly. The decision to update the packet fields is based on the whether 
// the packet is an HTTP Get request to the protected web server.
//
//  This module uses the below formula to update the TCP checksum:
//  new_checksum = ~(~old_checksum + ~old_value +  new_value)
//
// This module is located in User Data Path between HADAM Puzzle Setter and
// Output Queues modules.
//
//
///////////////////////////////////////////////////////////////////////////////
`timescale 1ns/1ps
module  hadam_pktfield_changer
   #(
      parameter DATA_WIDTH = 64,
      parameter CTRL_WIDTH = DATA_WIDTH/8,
      parameter UDP_REG_SRC_WIDTH = 2,
      parameter INPUT_ARBITER_STAGE_NUM = 2,
      parameter NUM_OUTPUT_QUEUES = 8,
      parameter STAGE_NUM = 5,
      parameter NUM_IQ_BITS = 3
   )
   (
      // --- data path interface
      output reg [DATA_WIDTH-1:0]        out_data,
      output reg [CTRL_WIDTH-1:0]        out_ctrl,
      output reg                         out_wr,
      input                              out_rdy,

      input  [DATA_WIDTH-1:0]            in_data,
      input  [CTRL_WIDTH-1:0]            in_ctrl,
      input                              in_wr,
      output                             in_rdy,

	// -- is HTTP GET from http_get_filter --> output_Port_lookup
      input 				is_http_get,


      // --- Preprocess interface
	input [15:0] 		TCP_checksum,
	input [15:0]		IP_checksum,	
	input [31:0]		src_ip_old,
	input [31:0]		dst_ip_old,
	input [15:0]		src_port_old,
	input [15:0]		dst_port_old,
	input [15:0]		win_old,
	input [31:0] 	        seq_no_old, 
      	input [31:0]	        ack_no_old,
	input			preprocess_vld,

	output reg                         rd_preprocess_info,

      // --- Register interface
      input                              reg_req_in,
      input                              reg_ack_in,
      input                              reg_rd_wr_L_in,
      input  [`UDP_REG_ADDR_WIDTH-1:0]   reg_addr_in,
      input  [`CPCI_NF2_DATA_WIDTH-1:0]  reg_data_in,
      input  [UDP_REG_SRC_WIDTH-1:0]     reg_src_in,

      output                             reg_req_out,
      output                             reg_ack_out,
      output                             reg_rd_wr_L_out,
      output  [`UDP_REG_ADDR_WIDTH-1:0]  reg_addr_out,
      output  [`CPCI_NF2_DATA_WIDTH-1:0] reg_data_out,
      output  [UDP_REG_SRC_WIDTH-1:0]    reg_src_out,
      

      // --- Misc
      input                              clk,
      input                              reset
   );

   `LOG2_FUNC
   
   //--------------------- Internal Parameter-------------------------
   localparam NUM_STATES = 8;

   localparam SKIP_HDR    = 1;
   localparam WORD2_CHECK_IPV4 = 2;
   localparam WORD3_CHECK_TCP = 4;
   localparam WORD4_IP_ADDR            = 8;
   localparam WORD5_TCP_PORT     = 16;
   localparam WORD6_TCP_FLAGS           = 32;
   localparam WORD7_TCP_CHECKSUM            = 64;
   localparam PAYLOAD            = 128;

	//rgp added
	//localparam is_http_get = 1;
	localparam is_traffic_high = 1; 


   //---------------------- Wires and regs----------------------------

   reg                              in_fifo_rd_en;
   wire [CTRL_WIDTH-1:0]            in_fifo_ctrl_dout;
   wire [DATA_WIDTH-1:0]            in_fifo_data_dout;
   wire                             in_fifo_nearly_full;
   wire                             in_fifo_empty;


   reg [NUM_STATES-1:0]             state;
   reg [NUM_STATES-1:0]             state_next;
   
   reg [20:0]			    checksum,checksum_next, checksum_old;
   reg  			    match, match_next;
   reg				    check_IP, check_TCP, check_TCP_next;
   wire [31:0] 		            dst_ip, src_ip, dst_port, src_port, win, src_ip_init, dst_ip_init, dst_port_init, src_port_init;
   // rgp added	
   reg [31:0] 		            dst_ip_frm_frp, src_ip_frm_frp;
   reg [15:0]                       dst_port_frm_frp, src_port_frm_frp, win_frm_frp;
   reg [20:0]			    new_TCP_chksm_l,new_TCP_chksm_l_next; 
   reg [31:0]			    new_ack_no, new_ack_no_next;		

	 
//------------------------- Modules-------------------------------
        //Input data from previous module
fallthrough_small_fifo #(.WIDTH(DATA_WIDTH+CTRL_WIDTH), .MAX_DEPTH_BITS(8))
      input_fifo
        (.din ({in_ctrl,in_data}),     // Data in
         .wr_en (in_wr),               // Write enable
         .rd_en (in_fifo_rd_en),       // Read the next word 
         .dout ({in_fifo_ctrl_dout, in_fifo_data_dout}),
         .full (),
         .nearly_full (in_fifo_nearly_full),
         .empty (in_fifo_empty),
         .reset (reset),
         .clk (clk)
         );

	//Module used in order to access to HADAM register define in /include/hadam.xml
generic_regs
   #( 
      .UDP_REG_SRC_WIDTH   (UDP_REG_SRC_WIDTH),
      .TAG                 (`HADAM_BLOCK_ADDR),
      .REG_ADDR_WIDTH      (`HADAM_REG_ADDR_WIDTH),                       // Width of block addresses
      .NUM_COUNTERS        (0),                       // How many counters
      .NUM_SOFTWARE_REGS   (9),                       // How many sw regs
      .NUM_HARDWARE_REGS   (0)                        // How many hw regs
   ) hadam_regs (
      .reg_req_in       (reg_req_in),
      .reg_ack_in       (reg_ack_in),
      .reg_rd_wr_L_in   (reg_rd_wr_L_in),
      .reg_addr_in      (reg_addr_in),
      .reg_data_in      (reg_data_in),
      .reg_src_in       (reg_src_in),

      .reg_req_out      (reg_req_out),
      .reg_ack_out      (reg_ack_out),
      .reg_rd_wr_L_out  (reg_rd_wr_L_out),
      .reg_addr_out     (reg_addr_out),
      .reg_data_out     (reg_data_out),
      .reg_src_out      (reg_src_out),

      // --- counters interface
      .counter_updates  (),
      .counter_decrement(),

       // --- SW regs interface, NOTE: those values are returned in reverse order with respect to the register definitions in hadam.xml
      .software_regs    ({dst_port_init, src_port_init, dst_ip_init, src_ip_init, win, dst_port, src_port, dst_ip, src_ip}),

      // --- HW regs interface
      .hardware_regs    (),

      .clk              (clk),
      .reset            (reset)
    );

   //----------------------- Logic -----------------------------

   assign    in_rdy = !in_fifo_nearly_full;
  
   always @(*) begin
      out_ctrl = in_fifo_ctrl_dout;
      out_data = in_fifo_data_dout;
      state_next = state;
      checksum_next =checksum;
      match_next = match;	
      check_TCP_next = check_TCP;
      new_TCP_chksm_l_next=new_TCP_chksm_l; 
      new_ack_no_next=new_ack_no;		
      rd_preprocess_info            = 0;
      in_fifo_rd_en = 0;
      out_wr = 0;


      case(state)
            // Skip all control headers and first packet word
            SKIP_HDR: begin
             if(preprocess_vld) begin
		  // Wait for data to be in the FIFO and the output to be ready
               if (!in_fifo_empty && out_rdy) begin
                  out_wr = 1;
                  in_fifo_rd_en = 1;
                  if (in_fifo_ctrl_dout == 'h0) begin
			if (is_http_get && is_traffic_high) begin
				out_data [63:16]= 'h00270e2ea638;
			end                     
			state_next = WORD2_CHECK_IPV4;
                  end
               end
		end
            end // case: SKIP_HDR
   
	WORD2_CHECK_IPV4: begin
                //If not IPv4 packet, skip packet parsing, else initialize values and move to the next state
		if (!in_fifo_empty && out_rdy) begin
                        rd_preprocess_info =1;
			dst_ip_frm_frp=dst_ip_old;
			src_ip_frm_frp=src_ip_old;
			dst_port_frm_frp=dst_port_old;
			src_port_frm_frp=src_port_old;				
	 $display("FR OLD DST IP=, %h, SRC IP=, %h, DST_PORT=, %d, SRC_PORT=,%d",dst_ip_frm_frp, src_ip_frm_frp, dst_port_frm_frp, src_port_frm_frp );
			if (is_http_get && is_traffic_high) begin
				new_ack_no_next = seq_no_old + 'h286;
				out_data[47:32]='h0001;
			end
			out_wr = 1;
         		in_fifo_rd_en = 1;
			if(in_fifo_data_dout[15:12] != 4'h4) begin 
				state_next = PAYLOAD;	
			end
			else begin
          			state_next= WORD3_CHECK_TCP;
			end
		$display("FR GET_LEN, %h, proto=%d", state_next, in_fifo_data_dout[15:12]);    
		$display("FR IN_FIFO_DATA_OUT = %h", in_fifo_data_dout);          	

  end
          end // case: WORD2_CHECK_IPV4
          
	WORD3_CHECK_TCP: begin
                //If not TCP packet, skip packet parsing, else move to the next state
		if (!in_fifo_empty && out_rdy) begin
			out_wr = 1;
         	   	in_fifo_rd_en = 1;	    
         	 	if (in_fifo_data_dout[7:0] == 8'h6) begin
                		state_next = WORD4_IP_ADDR;
              		end
              		else begin
                		state_next= PAYLOAD;
              		end
        	$display("FR WAIT_TCP, %h %h", state_next, in_fifo_data_dout[7:0] );    
       		end
          end // case: WORD3_CHECK_TCP
                          
          WORD4_IP_ADDR: begin
		// If necessary write new source and destination IP address and new IP checksum 
		if (!in_fifo_empty && out_rdy) begin
                	out_wr = 1;
         	    	in_fifo_rd_en = 1;	    		
			if (is_http_get && is_traffic_high) begin
			        out_data[47:16] = dst_ip_frm_frp;               
               		        out_data[15:0]  = src_ip_frm_frp[31:16];
				// new TCP Checksum = ~old TCP Checksum Get Request + ~Get_req payload ones compliment Sum + Captcha Payload ones compliment sum + ~Get Req Seq No + New ack no
				// new_TCP_chksm_l_next[20:0] ={5'b0,~TCP_checksum} + 21'h85ef + 21'h5844 + {5'b0,~seq_no_old[31:16]} + {5'b0,new_ack_no[31:16]} + {5'b0,~seq_no_old[15:0]} + {5'b0,new_ack_no[15:0]};
				new_TCP_chksm_l_next[20:0] ={5'b0,~TCP_checksum} + 21'h85ef + 21'h5844 + {5'b0,~seq_no_old[31:16]} ;
				$display("New tcp checksum 21 bits  = %h",new_TCP_chksm_l_next);
				
				$display("New SRC IP = %h, Dest_IP_Hi= %h",out_data[47:16], out_data[15:0]);
			end
        	//$display("CHECK_SRC %h, %h", IP_checksum, out_data);   
		  state_next = WORD5_TCP_PORT; 
          	  end
          end // case: WORD4_IP_ADDR
                    
          WORD5_TCP_PORT: begin
		// If necessary write new destination IP address(low part), new source and destination TCP port 
		if (!in_fifo_empty && out_rdy) begin
                	out_wr = 1;
         	   	in_fifo_rd_en = 1;			

			if(is_http_get && is_traffic_high) begin
				 out_data[63:48] =  src_ip_frm_frp[15:0];
                                 out_data[47:32]  = dst_port_frm_frp ;
                                 out_data[31:16]  = src_port_frm_frp ;
				 out_data[15:0]  = ack_no_old[31:16];
				 new_TCP_chksm_l_next[20:0] =new_TCP_chksm_l+ {5'b0,new_ack_no[31:16]} + {5'b0,~seq_no_old[15:0]} + {5'b0,new_ack_no[15:0]};
				 $display("New DST IP LO = %h, Src_port= %h, Dst_port= %h ",out_data[63:48], out_data[47:32], out_data[31:16]);
				
			end

			state_next = WORD6_TCP_FLAGS;                     
        		//$display("WORD_NONE, %h ", state_next);    
			end
         	 end // case: WORD5_TCP_PORT
                      
          WORD6_TCP_FLAGS: begin
		// Wait for next word
		if (!in_fifo_empty && out_rdy) begin
                	out_wr = 1;
		    	in_fifo_rd_en = 1;
			if(is_http_get && is_traffic_high) begin	
				out_data[63:48] = ack_no_old[15:0];
				out_data[47:16] = new_ack_no;
				 new_TCP_chksm_l_next[20:0] = (new_TCP_chksm_l_next[15:0] + new_TCP_chksm_l_next[20:16]);
				 $display("After New tcp checksum 21 bits  = %h",new_TCP_chksm_l_next);
			end
            	state_next = WORD7_TCP_CHECKSUM;
        	//$display("CHECK_ACK, %h %h, tcp_length_wrd_next=%d, tcp_length_wrd = %d", state_next, sum_next,tcp_length_wrd_next,tcp_length_wrd);    
            	end
          end // case: WORD6_TCP_FLAGS
                      

          WORD7_TCP_CHECKSUM: begin
		// If necessary write new TCP window size andi/or TCP checksum
		if (!in_fifo_empty && out_rdy) begin
                	out_wr = 1;
         	   	in_fifo_rd_en = 1;
			if(is_http_get && is_traffic_high) begin
				out_data[47:32] = ~new_TCP_chksm_l[15:0];	
			end
		  	state_next = PAYLOAD;
		end
          end //case: WORD7_TCP_CHECKSUM 

            // Process and skip all data
            PAYLOAD: begin
               // Wait for data to be in the FIFO and the output to be ready
               if (!in_fifo_empty && out_rdy) begin
                  out_wr = 1;
                  in_fifo_rd_en = 1;
                   // Check for EOP
                  if (in_fifo_ctrl_dout != 'h0) begin
                     state_next = SKIP_HDR;
                  end
               end
            end // case: PAYLOAD
   
         endcase // case(state)
   end // always @ (*)

   always @(posedge clk) begin
	if(reset) begin
		state <= SKIP_HDR;
		match <= 0;
		new_TCP_chksm_l <=0; 
     		new_ack_no<=0;
	end
	else begin
		state <= state_next;
		checksum <= checksum_next;
		match <= match_next;
		check_TCP <= check_TCP_next;
		new_TCP_chksm_l <= new_TCP_chksm_l_next; 
                new_ack_no <= new_ack_no_next;
	end
     end // always @ (posedge clk)

endmodule // hadam_pktfield_changer
