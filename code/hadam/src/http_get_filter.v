///////////////////////////////////////////////////////////////////////////////
// Adapted from the URL Extraction Project in NetFPGA Projects bucket.
// <http://wiki.netfpga.org/foswiki/bin/view/NetFPGA/OneGig/URL>
// Author: Michael Ciesla <mick.ciesla@gmail.com>
// Date: 21st July 2009
// Module: http_get_filter.v
// Project: HADAM
// Description: Determines if a packet carries a HTTP GET Request and signals to 
//              process block.
//
///////////////////////////////////////////////////////////////////////////////
`timescale 1ns/100ps
  module http_get_filter #(parameter DATA_WIDTH = 64,
      parameter NUM_QUEUES = 8,
      parameter NUM_QUEUES_WIDTH = log2(NUM_QUEUES)
      )
   (// --- Interface to the previous stage
    input  [DATA_WIDTH-1:0]            in_data,
    input                              in_wr,


    // --- Interface to process block
    output                             is_http_get_pkt,
    input                              http_get_rd_info,
    output                             http_get_info_vld,

    // --- Interface to preprocess block
    input                              word_IP_LEN_ID,

    // --- Misc
    input                              reset,
    input                              clk
   );

   function integer log2;
      input integer number;
      begin
         log2=0;
         while(2**log2<number) begin
            log2=log2+1;
         end
      end
   endfunction // log2

   //------------------ Internal Parameter ---------------------------

   localparam WAIT_IP_PROTO_LEN = 1;
   localparam WORD_3            = 2;
   localparam CHECK_DST_PORT    = 4;
   localparam CHECK_TCP_LEN     = 8;
   localparam WIN_GET           = 16;
   localparam WORD_7            = 32;
   localparam UNIX_GET          = 64;

   localparam TCP                = 8'h06;
   localparam MIN_LEN            = 16'd56;      // Just enough length for the "GET " in a unix pkt.
   localparam HTTP               = 16'h0050;    // port 80
   localparam GE                 = 16'h4745;    // "GE"
   localparam GET                = 24'h474554;  // "GET"
   localparam WIN_TCP_HDR_LEN    = 4'b0101;     // 5 * 32b = 20B
   //---------------------- Wires/Regs -------------------------------
   
   reg [9:0]                              state, state_next;
   reg                                    wr_en;
   reg                                    is_get_pkt;
   reg                                    win_pkt, win_pkt_next;

   wire [3:0]                             tcp_hdr_len;
   wire [7:0]                             ip_proto;
   wire [15:0]                            ip_len;
   wire [15:0]                            dst_port;
   wire [15:0]                            req_meth_win;
   wire [23:0]                            req_meth_unix;

   //----------------------- Modules ---------------------------------
   fallthrough_small_fifo #(.WIDTH(1), .MAX_DEPTH_BITS(2))
      http_get_fifo
        (.din (is_get_pkt),         
         .wr_en (wr_en),                        // Write enable
         .rd_en (http_get_rd_info),             // Read the next word 
         .dout (is_http_get_pkt),
         .full (),
         .nearly_full (),
         .prog_full     (),
         .empty (empty),
         .reset (reset),
         .clk (clk)
         );   
       
   //------------------------ Logic ----------------------------------
   assign http_get_info_vld = !empty;
   assign req_meth_unix = in_data[47:24];  
   assign req_meth_win = in_data[15:0];  
   assign ip_proto = in_data[7:0];
   assign ip_len = in_data[63:48];
   assign dst_port = in_data[31:16];
   assign tcp_hdr_len = in_data[15:12];

   always@(*) begin
      state_next = state;
      win_pkt_next = win_pkt;
      wr_en = 0;
      is_get_pkt = 0;

      case(state)
         WAIT_IP_PROTO_LEN: begin
            win_pkt_next = 0;
            if (word_IP_LEN_ID) begin
               /* need to check pkt is long enough to contain a GET string */
               if (ip_len > MIN_LEN && ip_proto == TCP) begin
                  state_next = WORD_3;
               end
               else begin
                  wr_en = 1;
               end
            end
         end

         WORD_3: begin
            if (in_wr) begin
               state_next = CHECK_DST_PORT;
            end
         end

         CHECK_DST_PORT: begin
            if (in_wr) begin
               if (dst_port == HTTP) begin
                  state_next = CHECK_TCP_LEN;
               end
               else begin
                  state_next = WAIT_IP_PROTO_LEN;
                  wr_en = 1;
               end
            end
         end

         CHECK_TCP_LEN: begin
            if (in_wr) begin
               if (tcp_hdr_len == WIN_TCP_HDR_LEN) begin
                  win_pkt_next = 1;
               end
               state_next = WIN_GET;
            end
         end

         WIN_GET: begin
            if (in_wr) begin
               if (win_pkt) begin
                  if (req_meth_win == GE) begin
                     is_get_pkt = 1;
                  end
                  wr_en = 1;
                  /* stop searching if its a windows packet */
                  state_next = WAIT_IP_PROTO_LEN;
               end
               else begin
                  state_next = WORD_7;
               end
            end
         end

         WORD_7: begin
         if (in_wr) begin
               state_next = UNIX_GET;
            end
         end

         UNIX_GET: begin
            if (in_wr) begin
               if (req_meth_unix == GET) begin
                  is_get_pkt = 1;
               end
               wr_en = 1;
               state_next = WAIT_IP_PROTO_LEN;
            end
         end

      endcase // case(state)
   end // always@ (*)

   always @(posedge clk) begin
      if(reset) begin
         state <= WAIT_IP_PROTO_LEN;
         win_pkt <= 0;
      end
      else begin
         state <= state_next;
         win_pkt <= win_pkt_next;
      end
   end
   
endmodule // http_get_filter

