///////////////////////////////////////////////////////////////////////////////
//
// Author: Prajith R G
// Module: hadam_puzzle_setter.v
// Project: HADAM
// Description: Extracts the IP and TCP information from the incoming packet
// if its a HTTP GET Packet targeting the protected web server and outputs it
// to the HADAM PKT FIELD Changer Module. This module replaces the payload of
// the incoming HTTP Get packet with the Puzzle payload. Here Length is
// a constraint to satisfy the NetFPGA pipeline, so its assumed that the
// length of the Puzzle payload is less than or equal to the length of the
// HTTP Get request payload. If its less zeroes are padded.
//
// This module is located in User Data Path between Output Port Lookup and 
// HADAM PktField Changer modules.
//
///////////////////////////////////////////////////////////////////////////////
`timescale 1ns/1ps
module hadam_puzzle_setter
   #(
      parameter DATA_WIDTH = 64,
      parameter CTRL_WIDTH = DATA_WIDTH/8,
      parameter UDP_REG_SRC_WIDTH = 2,
      parameter INPUT_ARBITER_STAGE_NUM = 2,
      parameter NUM_OUTPUT_QUEUES = 8,
      parameter STAGE_NUM = 5,
      parameter NUM_IQ_BITS = 3
   )
   (
      // --- data path interface
      output reg [DATA_WIDTH-1:0]        out_data,
      output reg [CTRL_WIDTH-1:0]        out_ctrl,
      output reg                         out_wr,
      input                              out_rdy,

      input  [DATA_WIDTH-1:0]            in_data,
      input  [CTRL_WIDTH-1:0]            in_ctrl,
      input                              in_wr,
      output                             in_rdy,

	// -- is HTTP GET from http_get_filter --> output_Port_lookup
      input 				is_http_get,

	// --- HADAM PktField Setter interface
      output [15:0]            TCP_checksum,
      output [15:0]            IP_checksum,
      output [31:0]            src_ip_old,
      output [31:0]            dst_ip_old,
      output [15:0]            src_port_old,
      output [15:0]            dst_port_old,
      output [15:0]            win_old,
      output [31:0] 	       seq_no_old, 
      output [31:0]	       ack_no_old,
      output     	       preprocess_vld,
      input 	    	       rd_result,

      // --- Register interface
      input                              reg_req_in,
      input                              reg_ack_in,
      input                              reg_rd_wr_L_in,
      input  [`UDP_REG_ADDR_WIDTH-1:0]   reg_addr_in,
      input  [`CPCI_NF2_DATA_WIDTH-1:0]  reg_data_in,
      input  [UDP_REG_SRC_WIDTH-1:0]     reg_src_in,

      output                             reg_req_out,
      output                             reg_ack_out,
      output                             reg_rd_wr_L_out,
      output  [`UDP_REG_ADDR_WIDTH-1:0]  reg_addr_out,
      output  [`CPCI_NF2_DATA_WIDTH-1:0] reg_data_out,
      output  [UDP_REG_SRC_WIDTH-1:0]    reg_src_out,
      

      // --- Misc
      input                              clk,
      input                              reset
   );

   `LOG2_FUNC
   
   //--------------------- Internal Parameter-------------------------
   localparam NUM_STATES = 8;

   localparam SKIP_HDR    = 1;
   localparam WORD2_CHECK_IPV4 = 2;
   localparam WORD3_CHECK_TCP = 4;
   localparam WORD4_IP_ADDR            = 8;
   localparam WORD5_TCP_PORT     = 16;
   localparam WORD6_TCP_FLAGS           = 32;
   localparam WORD7_TCP_CHECKSUM            = 64;
   localparam PAYLOAD            = 128;


   //---------------------- Wires and regs----------------------------

   reg                              in_fifo_rd_en;
   wire [CTRL_WIDTH-1:0]            in_fifo_ctrl_dout;
   wire [DATA_WIDTH-1:0]            in_fifo_data_dout;
   wire                             in_fifo_nearly_full;
   wire                             in_fifo_empty;


   reg [NUM_STATES-1:0]             state;
   reg [NUM_STATES-1:0]             state_next;
   
   reg [20:0]	 		    TCP_checksum_old, TCP_checksum_old_next, old_payload_chksm, old_payload_chksm_next, new_payload_chksm_next, new_payload_chksm;
   reg                              wr_en;
   wire				    empty; 

   wire [31:0]			    dst_ip;
   reg [31:0] 			    src_ip_old_l, src_ip_old_l_next, dst_ip_old_l, dst_ip_old_l_next, seq_no_old_l, seq_no_old_l_next, ack_no_old_l, ack_no_old_l_next ;
   reg [15:0] 			    src_port_old_l, src_port_old_l_next, dst_port_old_l, dst_port_old_l_next;
   reg 				    check_TCP,check_TCP_next, check_IP, check_IP_next, check_test;     
  


   reg [71:0] captcha_payload;

   reg [7:0] cap_cnt,cap_cnt_next ;

   reg flag;
 
//------------------------- Modules-------------------------------
	//Input data from previous module
fallthrough_small_fifo #(.WIDTH(DATA_WIDTH+CTRL_WIDTH), .MAX_DEPTH_BITS(2))
      input_fifo
        (.din ({in_ctrl,in_data}),     // Data in
         .wr_en (in_wr),               // Write enable
         .rd_en (in_fifo_rd_en),       // Read the next word 
         .dout ({in_fifo_ctrl_dout, in_fifo_data_dout}),
         .full (),
         .nearly_full (in_fifo_nearly_full),
         .empty (in_fifo_empty),
         .reset (reset),
         .clk (clk)
         );

        //Output data to next module
fallthrough_small_fifo #(.WIDTH(208), .MAX_DEPTH_BITS(10))
      output_fifo
        (
        .din ({TCP_checksum_old_next[15:0],old_payload_chksm[15:0], src_ip_old_l, dst_ip_old_l, src_port_old_l, dst_port_old_l, new_payload_chksm[15:0],seq_no_old_l, ack_no_old_l}),
         .wr_en (wr_en),                        // Write enable
         .rd_en (rd_result),             // Read the next word
         .dout ({TCP_checksum,IP_checksum, src_ip_old, dst_ip_old, src_port_old, dst_port_old, win_old, seq_no_old, ack_no_old}),
         .full (),
         .nearly_full (),
         .prog_full     (),
         .empty (empty),
         .reset (reset),
         .clk (clk)
         );

	//Module used in order to access to HADAM register define in /include/hadam.xml
generic_regs
   #( 
      .UDP_REG_SRC_WIDTH   (UDP_REG_SRC_WIDTH),
      .TAG                 (`HADAM_BLOCK_ADDR),
      .REG_ADDR_WIDTH      (`HADAM_REG_ADDR_WIDTH),                       // Width of block addresses
      .NUM_COUNTERS        (0),                       // How many counters
      .NUM_SOFTWARE_REGS   (9),                       // How many sw regs
      .NUM_HARDWARE_REGS   (0)                        // How many hw regs
   ) hadam_regs (
      .reg_req_in       (reg_req_in),
      .reg_ack_in       (reg_ack_in),
      .reg_rd_wr_L_in   (reg_rd_wr_L_in),
      .reg_addr_in      (reg_addr_in),
      .reg_data_in      (reg_data_in),
      .reg_src_in       (reg_src_in),

      .reg_req_out      (reg_req_out),
      .reg_ack_out      (reg_ack_out),
      .reg_rd_wr_L_out  (reg_rd_wr_L_out),
      .reg_addr_out     (reg_addr_out),
      .reg_data_out     (reg_data_out),
      .reg_src_out      (reg_src_out),

      // --- counters interface
      .counter_updates  (),
      .counter_decrement(),

      // --- SW regs interface, NOTE: those values are returned in reverse order with respect to the register definitions in hadam.xml
      .software_regs    ({dst_port_init, src_port_init, dst_ip_init, src_ip_init, win, dst_port, src_port, dst_ip, src_ip}),

      // --- HW regs interface
      .hardware_regs    (),

      .clk              (clk),
      .reset            (reset)
    );

   //----------------------- Logic -----------------------------

   assign    in_rdy = !in_fifo_nearly_full;
   assign preprocess_vld = !empty;


   always @(*) begin
       captcha_payload= 'h0;
      case(cap_cnt)
	0:    captcha_payload=in_fifo_data_dout;
        1:    captcha_payload={in_fifo_data_dout[63:48],48'h485454502f31};
        2:    captcha_payload=   'h2e3120323030204f;	3:    captcha_payload=   'h4b0d0a446174653a;
	4:    captcha_payload=   'h204d6f6e2c203237;	5:    captcha_payload=   'h204d617920323031;
	6:    captcha_payload=   'h332032313a33303a;	7:    captcha_payload=   'h343520474d540d0a;
	8:    captcha_payload=   'h5365727665723a20;	9:    captcha_payload=   'h4170616368652f32;
	10:   captcha_payload=   'h2e322e3233202846;	11:   captcha_payload=   'h65646f7261290d0a;
	12:   captcha_payload=   'h4c6173742d4d6f64;	13:   captcha_payload=   'h69666965643a204d;
	14:   captcha_payload=   'h6f6e2c203237204d;	15:   captcha_payload=   'h6179203230313320;
	16:   captcha_payload=   'h32313a32393a3539;	17:   captcha_payload=   'h20474d540d0a4554;
	18:   captcha_payload=   'h61673a2022376665;    19:   captcha_payload=   'h38652d3136392d34;
	20:   captcha_payload=   'h6464623964396432;	21:   captcha_payload=   'h35623961220d0a41;
	23:   captcha_payload=   'h63636570742d5261;	24:   captcha_payload=   'h6e6765733a206279;
	25:   captcha_payload=   'h7465730d0a436f6e;	26:   captcha_payload=   'h74656e742d4c656e;
	27:   captcha_payload=   'h6774683a20333631;	28:   captcha_payload=   'h0d0a436f6e6e6563;
	29:   captcha_payload=   'h74696f6e3a20636c;	30:   captcha_payload=   'h6f73650d0a436f6e;
	31:   captcha_payload=   'h74656e742d547970;	32:   captcha_payload=   'h653a20746578742f;
	33:   captcha_payload=   'h68746d6c3b206368;	34:   captcha_payload=   'h61727365743d5554;
	35:   captcha_payload=   'h462d380d0a0d0a3c;	36:   captcha_payload=   'h21444f4354595045;
	37:   captcha_payload=   'h2068746d6c3e0a3c;	38:   captcha_payload=   'h68746d6c3e0a093c;
	39:   captcha_payload=   'h666f726d206d6574;	40:   captcha_payload=   'h686f643d22474554;
	41:   captcha_payload=   'h2220616374696f6e;	42:   captcha_payload=   'h3d222f76616c6964;
	43:   captcha_payload=   'h61746522203e0a09;	44:   captcha_payload=   'h094f757220776562;
	45:   captcha_payload=   'h7369746520697320;	46:   captcha_payload=   'h657870657269656e;
	47:   captcha_payload=   'h63696e6720756e75;	48:   captcha_payload=   'h7375616c6c792068;
	49:   captcha_payload=   'h696768206c6f6164;	50:   captcha_payload=   'h2e20536f2c207072;
	51:   captcha_payload=   'h6f76652075207220;	52:   captcha_payload=   'h612068756d616e20;
	53:   captcha_payload=   'h616e64206e6f7420;	54:   captcha_payload=   'h6120626f742e0909;
	55:   captcha_payload=   'h0a09093c62723e50;	56:   captcha_payload=   'h6c6561736520656e;
	57:   captcha_payload=   'h7465722074686520;	58:   captcha_payload=   'h636f6465203e203c;
	59:   captcha_payload=   'h666f6e7420636f6c;	60:   captcha_payload=   'h6f723d2272656422;
	61:   captcha_payload=   'h3e31323334353c2f;	62:   captcha_payload=   'h666f6e743e0a0909;
	63:   captcha_payload=   'h3c696e7075742074;	64:   captcha_payload=   'h7970653d22706173;
	65:   captcha_payload=   'h776f726422206e61;	66:   captcha_payload=   'h6d653d22414e5357;
	67:   captcha_payload=   'h4552223e0a09093c;	68:   captcha_payload=   'h696e707574207479;
	69:   captcha_payload=   'h70653d2268696464;	70:   captcha_payload=   'h656e22206e616d65;
	71:   captcha_payload=   'h3d22544f4b454e22;	72:   captcha_payload=   'h2076616c75653d22;
	73:   captcha_payload=   'h5b5d223e0a09093c;	74:   captcha_payload=   'h696e707574207479;
	75:   captcha_payload=   'h70653d227375626d;	76:   captcha_payload=   'h6974222076616c75;
	77:   captcha_payload=   'h653d225375626d69;	78:   captcha_payload=   'h74223e090a093c2f;
	79:   captcha_payload=   'h666f726d3e0a090a;   	80:   captcha_payload=   'h3c2f68746d6c3e0a;
	default : captcha_payload='h0;
      endcase // case(cap_cnt)
   end


 
   always @(*) begin
      out_ctrl = in_fifo_ctrl_dout;
      out_data = in_fifo_data_dout;
      state_next = state;
      src_ip_old_l_next = src_ip_old_l; 
      dst_ip_old_l_next = dst_ip_old_l; 
      src_port_old_l_next = src_port_old_l; 
      dst_port_old_l_next = dst_port_old_l; 
      wr_en =0;
      in_fifo_rd_en = 0;
      out_wr = 0;
      cap_cnt_next = cap_cnt;
      new_payload_chksm_next= new_payload_chksm;
      old_payload_chksm_next= old_payload_chksm;
      TCP_checksum_old_next=TCP_checksum_old;
      seq_no_old_l_next = seq_no_old_l;  
      ack_no_old_l_next = ack_no_old_l;

      case(state)
          // Skip all control headers and first packet word
         SKIP_HDR: begin
		if (in_fifo_empty && flag==1) begin 
			$display ("old_payload_chksm = %h", old_payload_chksm +in_fifo_data_dout[63:48] + in_fifo_data_dout[47:32] + in_fifo_data_dout[31:16] + in_fifo_data_dout[15:0]);
 		 $display ("new_payload_chksm = %h", new_payload_chksm+'h2); 
			flag=0;		
		end
               // Wait for data to be in the FIFO and the output to be ready
               if (!in_fifo_empty && out_rdy) begin
				out_wr = 1;
				if (in_fifo_ctrl_dout == 'h0) begin
			          	state_next =WORD2_CHECK_IPV4;
              			end
	                  	in_fifo_rd_en = 1;
				flag=0;	
	       end
            end // case: SKIP_HDR
   
	WORD2_CHECK_IPV4: begin
		//If not IPv4 packet, skip checksum calculation, else initialize values and move to the next state
		if (!in_fifo_empty && out_rdy) begin
    	    		out_wr = 1;
         		in_fifo_rd_en = 1;
			if(in_fifo_data_dout[15:12] != 4'h4) begin 
				state_next = PAYLOAD;	
				wr_en=1;
			end
			else begin
          			state_next= WORD3_CHECK_TCP;
				old_payload_chksm_next=0;
				new_payload_chksm_next=0;
			end
		$display("GET_LEN, %h, proto=%d", state_next, in_fifo_data_dout[15:12]);    
          	  end
          end	// case: WORD2_CHECK_IPV4
          
	WORD3_CHECK_TCP: begin
		//If not TCP packet, skip checksum calculation, else move to the next state
		if (!in_fifo_empty && out_rdy) begin
			out_wr = 1;
         	   	in_fifo_rd_en = 1;	      
			if (in_fifo_data_dout[7:0] == 8'h6) begin
                		state_next = WORD4_IP_ADDR;
              		end
              		else begin
                		state_next= PAYLOAD;
              			wr_en=1;
			end
        	$display("WAIT_TCP, %h %h", state_next, in_fifo_data_dout[7:0] );    
       		end
          end	// case: WORD3_CHECK_TCP
                          
          WORD4_IP_ADDR: begin
		// Get initial value from packet. 
		if (!in_fifo_empty && out_rdy) begin
                	out_wr = 1;
         	    	in_fifo_rd_en = 1;	    		
			//IP_checksum_old_next = ~in_fifo_data_dout[63:48];
         	    	src_ip_old_l_next = in_fifo_data_dout[47:16];
			dst_ip_old_l_next[31:16] = in_fifo_data_dout[15:0];
        		state_next = WORD5_TCP_PORT;
        	$display("CHECK_SRC %h", state_next);    
          	  end
          end // case: WORD4_IP_ADDR
                    
          WORD5_TCP_PORT: begin
		// Get initial value from packet. 
		if (!in_fifo_empty && out_rdy) begin
                	out_wr = 1;
         	   	in_fifo_rd_en = 1;			
			dst_ip_old_l_next[15:0] = in_fifo_data_dout[63:48];
			src_port_old_l_next = in_fifo_data_dout[47:32];
			dst_port_old_l_next = in_fifo_data_dout[31:16];
			seq_no_old_l_next [31:16] = in_fifo_data_dout[15:0];
			state_next = WORD6_TCP_FLAGS;                     
	        	$display("WORD_NONE, %h ", state_next);    
			$display("DEST %h", ~dst_ip_old_l_next);
			end
         	 end // case: WORD5_TCP_PORT
                      
          WORD6_TCP_FLAGS: begin
		// If necessary proceed with checksum calculation
		if (!in_fifo_empty && out_rdy) begin
                	out_wr = 1;
		    	in_fifo_rd_en = 1;
			seq_no_old_l_next [15:0] = in_fifo_data_dout[63:48];
			ack_no_old_l_next = in_fifo_data_dout[47:16];
         	   	state_next = WORD7_TCP_CHECKSUM;
                        $display("CHECK_ACK, %h", state_next);    
            	end
          end // case: WORD6_TCP_FLAGS
                      

          WORD7_TCP_CHECKSUM: begin
		// If necessary conclude TCP cecksum calculation 
		if (!in_fifo_empty && out_rdy) begin
                	out_wr = 1;
         	   	in_fifo_rd_en = 1;			
			TCP_checksum_old_next= in_fifo_data_dout[47:32];	
                	$display("original TCP checksum = %h ", TCP_checksum_old_next);
		  	state_next = PAYLOAD;
			wr_en=1;
		end
          end 

            // Process and skip all data
            PAYLOAD: begin
               // Wait for data to be in the FIFO and the output to be ready
$display("Inside Payload 1");
			if (!in_fifo_empty && out_rdy) begin
				out_wr = 1;
  		        	in_fifo_rd_en = 1;
			        old_payload_chksm_next = old_payload_chksm + in_fifo_data_dout[63:48] + in_fifo_data_dout[47:32] + in_fifo_data_dout[31:16] + in_fifo_data_dout[15:0];	
             
				if (is_http_get && in_fifo_ctrl_dout == 'h0) begin
                                         out_data = captcha_payload[63:0];
                                         out_ctrl = captcha_payload[71:64];
                                         cap_cnt_next=cap_cnt+1;
                        		 new_payload_chksm_next= new_payload_chksm + captcha_payload[63:48] + captcha_payload[47:32] + captcha_payload[31:16] + captcha_payload[15:0];
				end
				else if (is_http_get && in_fifo_ctrl_dout != 'h0) begin
					flag=1; 
					out_data = 'h2;
			               	out_ctrl = 'h1;  
			                cap_cnt_next = 0;
                                        state_next = SKIP_HDR;
					//wr_en=1;
					
				end                 	
				// Check for EOP
 		                else if (!is_http_get && in_fifo_ctrl_dout != 'h0) begin
					 state_next = SKIP_HDR;
					 //wr_en=1;
				end
                        end		
             end // case: PAYLOAD
   
         endcase // case(state)
   end // always @ (*)

   always @(posedge clk) begin
	if(reset) begin
		state <= SKIP_HDR;
		src_ip_old_l <= 0;
        	dst_ip_old_l <= 0;
       		src_port_old_l <= 0;
         	dst_port_old_l <= 0;
		TCP_checksum_old <= 0;
		ack_no_old_l <= 0;
		seq_no_old_l <=0;
		cap_cnt <= 0;
		old_payload_chksm <= 0;
		new_payload_chksm <= 0;
	end
	else begin
		state <= state_next;
		src_ip_old_l <= src_ip_old_l_next;
		dst_ip_old_l <= dst_ip_old_l_next;
		src_port_old_l <= src_port_old_l_next;
		dst_port_old_l <= dst_port_old_l_next;
		TCP_checksum_old <= TCP_checksum_old_next;
		ack_no_old_l <= ack_no_old_l_next;
		seq_no_old_l <= seq_no_old_l_next;
		cap_cnt <= cap_cnt_next;
		old_payload_chksm <= old_payload_chksm_next;
		new_payload_chksm <= new_payload_chksm_next;
	end
     end //always @ (posedge clk)

endmodule // hadam_puzzle_setter 
