#!/bin/env python

from NFTest import *
import random
from RegressRouterLib import *
import NFTest.simReg

def generate_load(length):
    load = ''
    for i in range(length):
        load += chr(randint(0,255))
    return load

phy2loop0 = ('../connections/2phy', [])

nftest_init(sim_loop = [], hw_config = [phy2loop0])
nftest_start()

routerMAC = ["00:ca:fe:00:00:01", "00:ca:fe:00:00:02", "00:ca:fe:00:00:03", "00:ca:fe:00:00:04"]
routerIP = ["192.168.0.40", "192.168.1.40", "192.168.2.40", "192.168.3.40"]

# Clear all tables in a hardware test (not needed in software)
if isHW():
    nftest_invalidate_all_tables()

# Write the mac and IP addresses
for port in range(4):
    nftest_add_dst_ip_filter_entry (port, routerIP[port])
    nftest_set_router_MAC ('nf2c%d'%port, routerMAC[port])


subnetIP = ["192.168.101.2","192.168.101.1"]
subnetMask = ["255.255.255.255","255.255.255.255"]
nextHopIP = ["192.168.101.2","192.168.101.1"]
outPort = [0x4, 0x1]
nextHopMAC = ["00:21:cc:65:ed:a8", "00:27:0e:2e:a6:38"]

for port in range(1):
	
    # set parameters
    SA = "00:27:0E:2E:A6:38"
    DST_IP = "192.168.101.2";   #not in the lpm table
    SRC_IP = "192.168.101.1"
    VERSION = 0x4

    # Wrong mac destination
    DA = "00:ca:fe:00:00:01"
    for i in range(2):
	nftest_add_LPM_table_entry (i, subnetIP[i], subnetMask[i], nextHopIP[i], outPort[i])
    	nftest_add_ARP_table_entry(i, nextHopIP[i], nextHopMAC[i])

    # loop for 30 packets
    nftest_barrier()
   # for i in range(1)
    sent_pkt=scapy.Ether(dst=DA,src=SA)/scapy.IP(src=SRC_IP,dst=DST_IP)/scapy.TCP(sport=[59662],dport=[80],seq=1040332702,ack=680993240,urgptr=0,options=[('NOP', ''), ('NOP', ''), ('Timestamp',(10058221L, 0L))])/"GET /index.html HTTP/1.0 \n\n"/generate_load(646)
#	sent_pkt=scapy.rdpcap("/root/captcha_pcaps/just_captcha");
    nftest_send_phy('nf2c0', sent_pkt)
    exp_pkt=scapy.Ether(dst=nextHopMAC,src=routerMAC[1])/scapy.IP(src=SRC_IP,dst=DST_IP)/scapy.TCP(dport=[80])/"GET /index.html HTTP/1.0 \n\n"/generate_load(100)
    nftest_expect_phy('nf2c0', exp_pkt)

    sent_pkt=scapy.Ether(dst=DA,src=SA)/scapy.IP(src=SRC_IP,dst=DST_IP)/scapy.TCP(sport=[33564],dport=[80])/"GET /index.html HTTP/1.0 \n\n"/generate_load(20)
    #  sent_pkt=scapy.rdpcap("/root/captcha_pcaps/just_captcha");
    nftest_send_phy('nf2c0', sent_pkt)
    exp_pkt=scapy.Ether(dst=nextHopMAC,src=routerMAC[1])/scapy.IP(src=SRC_IP,dst=DST_IP)/scapy.TCP(dport=[80])/"GET /index.html HTTP/1.0 \n\n"/generate_load(100)
    nftest_expect_phy('nf2c0', exp_pkt)
	
    sent_pkt = make_IP_pkt(dst_MAC=DA, src_MAC=SA, dst_IP=DST_IP, src_IP=SRC_IP, pkt_len=3)
    exp_pkt = make_IP_pkt(dst_MAC=nextHopMAC, src_MAC=routerMAC[0],TTL = 63, dst_IP=DST_IP, src_IP=SRC_IP)
    exp_pkt[scapy.Raw].load = sent_pkt[scapy.Raw].load
    nftest_send_phy('nf2c%d'%port, sent_pkt);                                 
    nftest_expect_phy('nf2c1', exp_pkt);

    nftest_barrier()
    if not isHW():
        simReg.regDelay(10000)


nftest_finish()
