import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;


public class GUI extends JFrame implements ActionListener{
private JPanel labelPanel_init = new JPanel(new GridLayout(2,1,2,20));
private JPanel fieldPanel_init = new JPanel(new GridLayout(2,1,2,20));
/*
private JPanel labelPanel = new JPanel(new GridLayout(5, 1,2,4));
private JPanel fieldPanel = new JPanel(new GridLayout(5, 1,2,4));
*/	
private JTextField src_ip_init = new JTextField(10);
private JLabel src_ip_init_lab = new JLabel("Protected Server IP Address  ", JLabel.RIGHT);

/*
private JTextField dst_ip_init = new JTextField(14);
private JLabel dst_ip_init_lab = new JLabel("Initial Destination IP", JLabel.RIGHT);
private JTextField src_port_init = new JTextField(14);
private JLabel src_port_init_lab = new JLabel("Initial Source Port TCP", JLabel.RIGHT);
private JTextField dst_port_init = new JTextField(14);
private JLabel dst_port_init_lab = new JLabel("Initial Destination Port TCP", JLabel.RIGHT);
private JTextField src_ip = new JTextField(14);
private JLabel src_ip_lab = new JLabel("New Protected Server IP Address", JLabel.RIGHT);
private JTextField dst_ip = new JTextField(14);
private JLabel dst_ip_lab = new JLabel("New Destination IP", JLabel.RIGHT);
private JTextField src_port = new JTextField(14);
private JLabel src_port_lab = new JLabel("New Source Port TCP", JLabel.RIGHT);
private JTextField dst_port = new JTextField(14);
private JLabel dst_port_lab = new JLabel("New Destination Port TCP", JLabel.RIGHT);
private JTextField win = new JTextField(14);
private JLabel win_lab = new JLabel("New TCP Window Size", JLabel.RIGHT);
*/
private JMenuBar menuBar;
private JMenu menu;
private JMenuItem about;


private JButton read  = new JButton("Read");
private JButton write = new JButton("Write");
private JButton reset = new JButton("Reset");
private JButton info = new JButton("Info");



public static final int HADAM_SRC_IP_REG  =        0x2000200;
public static final int HADAM_DST_IP_REG  =        0x2000204;
public static final int HADAM_SRC_PORT_REG =       0x2000208;
public static final int HADAM_DST_PORT_REG =       0x200020c;
public static final int HADAM_WIN_REG  =           0x2000210;
public static final int HADAM_SRC_IP_INIT_REG  =   0x2000214;
public static final int HADAM_DST_IP_INIT_REG =    0x2000218;
public static final int HADAM_SRC_PORT_INIT_REG =  0x200021c;
public static final int HADAM_DST_PORT_INIT_REG =  0x2000220;


/*private long addr[] = {HADAM_SRC_IP_REG, HADAM_DST_IP_REG, HADAM_SRC_PORT_REG, HADAM_DST_PORT_REG, HADAM_WIN_REG, HADAM_SRC_IP_INIT_REG, HADAM_DST_IP_INIT_REG, HADAM_SRC_PORT_INIT_REG, HADAM_DST_PORT_INIT_REG,};  
*/
private long addr[] = {HADAM_SRC_IP_INIT_REG};  


public GUI(){
		super("HADAM");
		setSize(80,100);
		setLocation(20,20);
		Image icon = Toolkit.getDefaultToolkit().getImage("icon.jpg");
		this.setIconImage(icon);
		this.setResizable(false);
		Container c = getContentPane();
		JPanel pan= new JPanel();
		JPanel pan_w= new JPanel();
		JPanel buttonpan= new JPanel();
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(1,1,1,20));
		panel.add(pan_w);
		//panel.add(pan);
		c.add("Center",panel);
		c.add("South", buttonpan);

		
		src_ip_init.setEditable(true);
		
		/*
		dst_ip_init.setEditable(true);
		src_port_init.setEditable(true);
		dst_port_init.setEditable(true);
		src_ip.setEditable(true);
		dst_ip.setEditable(true);
		src_port.setEditable(true);
		dst_port.setEditable(true);
		win.setEditable(true);
		*/
	
		buttonpan.setLayout(new GridLayout(1,4,5,2));
		//pan.setLayout(new GridLayout(1,2,1,2));
		pan_w.setLayout(new GridLayout(1,1,1,2));
		buttonpan.add(read);
		buttonpan.add(write);
		buttonpan.add(reset);
		buttonpan.add(info);
		fieldPanel_init.add(src_ip_init);
		labelPanel_init.add(src_ip_init_lab);
		
		/*
		fieldPanel_init.add(dst_ip_init);
		fieldPanel_init.add(src_port_init);
		fieldPanel_init.add(dst_port_init);
		labelPanel_init.add(src_ip_init_lab);
		labelPanel_init.add(dst_ip_init_lab);
		labelPanel_init.add(src_port_init_lab);
		labelPanel_init.add(dst_port_init_lab);
		
		fieldPanel.add(src_ip);
		fieldPanel.add(dst_ip);
		fieldPanel.add(src_port);
		fieldPanel.add(dst_port);
		fieldPanel.add(win);
		labelPanel.add(src_ip_lab);
		labelPanel.add(dst_ip_lab);
		labelPanel.add(src_port_lab);
		labelPanel.add(dst_port_lab);
		labelPanel.add(win_lab);
		*/
		//pan.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
		pan_w.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
		buttonpan.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
	
			
		pan_w.add(labelPanel_init);	
		pan_w.add(fieldPanel_init);
/*
		pan.add(labelPanel);	
		pan.add(fieldPanel);
	*/	
		menuBar = new JMenuBar();

		menu = new JMenu("Help");
		menu.setMnemonic(KeyEvent.VK_H);
		menuBar.add(menu);

		about= new JMenuItem("About", KeyEvent.VK_A);
		about.addActionListener(this);
		menu.add(about);	
		setJMenuBar(menuBar);
		info.addActionListener(this);
		read.addActionListener(this);
		reset.addActionListener(this);
		write.addActionListener(this);
		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		readReg();
		setVisible(true);
		}
		
public void actionPerformed(ActionEvent ev){
	Object o = ev.getSource();
	if(o==info){
		JOptionPane.showMessageDialog(null, "HADAM GUI");
		}
	if(o==read){
		readReg();
	}
	if(o==write){	
		writeReg();
	}
	if(o==reset){	
		resetReg();
		readReg();
	}
	if(o==about){
		JOptionPane.showMessageDialog(null, "HADAM GUI");
	}
}

public void readReg(){
	String [] value = new String[9];
	try{
		for(int i=0; i<1; i++){
			Process p = Runtime.getRuntime().exec("regread " +  addr[i]);
                	InputStream is = p.getInputStream();
                	InputStreamReader isr = new InputStreamReader(is);
                	BufferedReader br = new BufferedReader(isr);
                	String line=null;
                	while ((line = br.readLine()) != null) {
                		value[i] = new String(line);
                	}
		}
		
		src_ip_init.setText(getDecIP(value[0].substring(value[0].lastIndexOf('x')+1,value[0].lastIndexOf(' '))));
		
		
		/*
		src_ip.setText(getDecIP(value[0].substring(value[0].lastIndexOf('x')+1,value[0].lastIndexOf(' '))));
		dst_ip.setText(getDecIP(value[1].substring(value[1].lastIndexOf('x')+1,value[1].lastIndexOf(' '))));
		src_port.setText(value[2].substring(value[2].lastIndexOf('(')+1,value[2].length()-1));
		dst_port.setText(value[3].substring(value[3].lastIndexOf('(')+1,value[3].length()-1));
		win.setText(value[4].substring(value[4].lastIndexOf('(')+1,value[4].length()-1));
		src_ip_init.setText(getDecIP(value[5].substring(value[5].lastIndexOf('x')+1,value[5].lastIndexOf(' '))));
		dst_ip_init.setText(getDecIP(value[6].substring(value[6].lastIndexOf('x')+1,value[6].lastIndexOf(' '))));
		src_port_init.setText(value[7].substring(value[7].lastIndexOf('(')+1,value[7].length()-1));
		dst_port_init.setText(value[8].substring(value[8].lastIndexOf('(')+1,value[8].length()-1));*/
	}
        catch(IOException io){}
}
	
public void writeReg(){
        String [] value = new String[9];
	value[0] = getHexIP(src_ip_init.getText());
        
	/*
	value[0] = getHexIP(src_ip.getText());
	
	value[1] = getHexIP(dst_ip.getText());
        value[2] = Long.toHexString(Long.parseLong(src_port.getText()));
        value[3] = Long.toHexString(Long.parseLong(dst_port.getText()));
	value[4] = Long.toHexString(Long.parseLong(win.getText()));
        value[5] = getHexIP(src_ip_init.getText());
        value[6] = getHexIP(dst_ip_init.getText());
        value[7] = Long.toHexString(Long.parseLong(src_port_init.getText()));
        value[8] = Long.toHexString(Long.parseLong(dst_port_init.getText()));
	*/
	try{
                for(int i=0; i<1; i++){
                        Process p = Runtime.getRuntime().exec("regwrite " +  addr[i] + " 0x" + value[i]);
                }
        }
        catch(IOException io){}
}

public void resetReg(){
        String [] value = new String[9];
        value[0] = getHexIP("0.0.0.0");
		
        /*value[1] = getHexIP("0.0.0.0");
        value[2] = Long.toHexString(Long.parseLong("0"));
        value[3] = Long.toHexString(Long.parseLong("0"));
        value[4] = Long.toHexString(Long.parseLong("0"));
        value[5] = getHexIP("0.0.0.0");
        value[6] = getHexIP("0.0.0.0");
        value[7] = Long.toHexString(Long.parseLong("0"));
        value[8] = Long.toHexString(Long.parseLong("0"));
*/
        try{
                for(int i=0; i<1; i++){
                        Process p = Runtime.getRuntime().exec("regwrite " +  addr[i] + " 0x" + value[i]);
                }
        }
        catch(IOException io){}
}


public static void main(String args[]){
	new GUI();
	}

public static String getDecIP(String hexIP) {
	int i=0;
	String ret = "";
	for(i=0;i<hexIP.length();i+=2) {
		char val[]=new char[2];
		val[0] = hexIP.charAt(i);
		val[1] = hexIP.charAt(i+1);
		String str = new String(val);
		ret += new Integer(Integer.parseInt(str,16)).toString();
		if(i<hexIP.length()-2)
			ret += ".";	
		}
	return ret;
	}
public static String getHexIP(String r) {
	String ret = "";
	String strsplitted[] = r.split("\\.");
	for(int i=0;i<strsplitted.length;i++) {
		String loc=Integer.toHexString(Integer.parseInt(strsplitted[i]));
		if(loc.length()==1)
			ret+="0";
		ret+=loc;
	}
	return ret;
}	
}
